import custService from "../../services";

export default {
  name: "AppHeader",
  components: {},
  props: [],
  data() {
    return {
      menus: [],
      topMenus: [],
      socials: [],
      CustService: new custService(),
      currentPage: this.$route.path,
      activeClass: "active"
    };
  },
  created() {
    var that = this;
    this.CustService.request("GET", "/header.json", null).then(
      res => {
        if (res.data) {
          that.menus = res.data.headerMenus;
          that.topMenus = res.data.topMenus;
          that.socials = res.data.socialLinks;
        }
      },
      err => {
        console.log(err);
      }
    );
  },
  mounted() {},

  methods: {
    changeFormat(string, type) {
      switch (type) {
        case "lower":
          return string.toLowerCase();
        default:
          return string;
      }
    },
    getClass(item) {
      let classes = [];
      classes.push("nav-item");
      if (this.$route.path === item.path) classes.push("active");
      // eslint-disable-next-line no-prototype-builtins
      if (item.hasOwnProperty("items") && item.items.length > 0)
        classes.push("dropdown");
      return classes.join(" ");
    },
    getIcon(item) {
      return "fa fa-" + item.icon;
    }
  }
};

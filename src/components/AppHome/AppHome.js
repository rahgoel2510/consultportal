import AppHomeCarousel from "../AppHomeCarousel/AppHomeCarousel.vue";
import AppQuote from "../AppQuote/AppQuote.vue";
export default {
  name: "AppHome",
  components: {
    AppHomeCarousel,
    AppQuote
  },
  props: [],
  data() {
    return {};
  },

  created() {},

  methods: {}
};

import "owl.carousel/dist/assets/owl.carousel.min.css";
import "animate.css";
import "owl.carousel/dist/owl.carousel.min.js";
export default {
  name: "AppHomeCarousel",
  components: {},
  props: [],
  data() {
    return {};
  },

  created() {},
  mounted() {
    // eslint-disable-next-line no-undef
    $(".owl-carousel").owlCarousel({
      loop: true,
      margin: 0,
      dots: false,
      nav: false,
      mouseDrag: false,
      autoplay: true,
      animateOut: "slideOutUp",
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    });
  },

  methods: {}
};

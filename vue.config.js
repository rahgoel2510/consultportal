module.exports = {
  lintOnSave: true,
  filenameHashing: true,
  configureWebpack: {
    performance: {
      hints: false
    },
    optimization: {
      splitChunks: {
        minSize: 512000,
        maxSize: 512000
      }
    }
  },
  css: {
    sourceMap: true
  }
};

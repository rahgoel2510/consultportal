import Vue from "vue";
import layout from "./layout.vue";
import router from "./router";
import store from "./store";
import "jquery/src/jquery.js";
import "popper.js/dist/popper.min.js";
import "bootstrap/dist/js/bootstrap.min.js";
import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
// import "roboto-fontface/css/roboto/roboto-fontface.css";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(layout)
}).$mount("#app");

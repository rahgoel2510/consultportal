import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../components/AppHome/AppHome.vue"),
    // redirect: { name: "Dashboard" },
    meta: {
      hidden: false,
      hasMulSub: false
    }
    // children: [
    //   {
    //     // path: "/dashboard",
    //     // name: "Dashboard",
    //     // component: AdApp,
    //     // meta: {
    //     //   icon: "dashboard"
    //     // }
    //   }
    // ]
  },
  {
    path: "/about",
    name: "about",
    component: () => import("../components/AppAbout/AppAbout.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  // eslint-disable-next-line no-unused-vars
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  }
});

export default router;
